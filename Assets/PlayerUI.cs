﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {
	[SerializeField] public bool shouldShowLocalPlayer = false;
	// Use this for initialization
    public Player player;

	public Text healthText;
	public Text staminaText;
	public Text shootCooldownText;
	public Text gerenadeCooldownText;

	void OnGUI() {
		if(!player) {
			return;
	    }
		this.GetComponent<Text>().text = player.playerTag;
		healthText.text = "HP:" + player.health;
		staminaText.text = "ST:" + player.CurrentStamina;
		shootCooldownText.text = "SCd:" + player.ShotCooldown;
		gerenadeCooldownText.text = "GCd:" + player.GerenadeCoolDown;
    }
}
