﻿using UnityEngine;
using UnityEngine.Networking;

public class Sword : NetworkBehaviour {
	// Use this for initialization
	[SyncVar(hook="OnTargetUpdate")]
	public Vector3 targetPosition;

	[SerializeField] float speed = 20;

	public AudioSource spawnSound;
	public AudioSource collisionSound;
	void Start () {
	  spawnSound.Play(0);
	}

    [SyncVar]
	private bool isScheduledForDestroy = false;

	void Update() {
		if(isScheduledForDestroy) {
			this.transform.position = this.transform.position + this.transform.forward.normalized/5;
        }
	}

	void OnTargetUpdate(Vector3 position) {
		this.targetPosition = position;
		this.transform.LookAt(position);
		this.GetComponent<Rigidbody>().velocity = this.transform.forward.normalized * speed;
	}
	void OnCollisionEnter (Collision collision) {
		var gerenade = collision.collider.GetComponent<Gerenade>();
		if(tag != collision.collider.tag && gerenade && isServer) {
			gerenade.Defuse();
			Destroy(this.gameObject);
		} else {
			ExecuteOnAll("OnHitUndestructable");
		}
    }

	void ExecuteOnAll(string method) {
		if(isServer) {
			Invoke(method, 0);
			RpcExecuteOnClient(method);
		}
	}

	[ClientRpc]
	void RpcExecuteOnClient(string method) {
		Invoke(method, 0);
	}

	void OnHitUndestructable() {
		var rg = this.GetComponent<Rigidbody>();
		rg.velocity = Vector3.zero;
		rg.drag = 0;
		rg.isKinematic = true;
		collisionSound.Play(0);
		if(isServer) {
            Invoke("ScheduleDestory", 0.3f);
		}
	}

	private void ScheduleDestory() {
		isScheduledForDestroy = true;
		Destroy(this.gameObject, 2);
	}
}
