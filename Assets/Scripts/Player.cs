﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {

    [SerializeField] float projectileLifeTime = 8;
    
    [SyncVar(hook="OnUpdatePlayerTag")]
    public string playerTag;

    [SyncVar]
    public int order;

    private string token;
    //Only on server
    private static List<string> playerTokens = new List<string>();

    int hitLayerMask;
    int destructableMask;

    public float CurrentStamina {get; private set;}
    public float health;

    public float ShotCooldown {
        get {
            var elapsed = Time.time - lastShootTime;
            return elapsed > mShootIntervalSec ? 0 : mShootIntervalSec - elapsed;
        }
    }
    public float GerenadeCoolDown {
       get {
            var elapsed = Time.time - lastGerenadeTime;
            return elapsed > mGerenadeIntervalSec ? 0 : mGerenadeIntervalSec - elapsed;
        }
    }
    [SerializeField] float mMaxStamina;
    [SerializeField] float mMaxHealth = 20;
    [SerializeField] float mShootStaminaCost;
    [SerializeField] float mGerenadeStaminaCost;
    [SerializeField] float mShootIntervalSec;

    private float lastShootTime = 0;
    private float lastGerenadeTime = 0;
    [SerializeField] float mGerenadeIntervalSec;
    [SerializeField] float mStaminaRegenPerSec;

    public GameObject swordPrefab;

    public GameObject[] gerenadePrefabs;

    public override void OnStartLocalPlayer() {
        if(string.IsNullOrEmpty(playerTag)) {
            Debug.Log("starting local, requesting player identity");
            var token = Guid.NewGuid().ToString();
            CmdRequestPlayerTag(token);
        }
    }

    public override void OnStartAuthority() {
        var UIs = GameObject.FindObjectsOfType<PlayerUI>();
        Debug.Log("OnStartAuthority" + UIs);
        foreach(var UI in UIs) {
            if(
                (hasAuthority && UI.shouldShowLocalPlayer) || 
                (!UI.shouldShowLocalPlayer && !hasAuthority))
            {
              UI.player = this.GetComponent<Player>();
            } 
        }
    }
    void Start () {
        hitLayerMask = 1 << LayerMask.NameToLayer ("Platform");
        CurrentStamina = mMaxStamina;
        health = mMaxHealth;
    }
    
    // Update is called once per frame
    void Update () {
        CurrentStamina += mStaminaRegenPerSec * Time.deltaTime;
        if(isClient && !hasAuthority) {
            return;
        }
        if(CurrentStamina > mMaxStamina) {
            CurrentStamina = mMaxStamina;
        }
        if(Input.GetButtonUp("Fire1")) {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if(Physics.Raycast(ray,out raycastHit, 500, destructableMask)) {
                CmdThrowKnife(raycastHit.point);
            } else if(Physics.Raycast(ray,out raycastHit, 500, hitLayerMask)) {
                CmdThrowGerenade(raycastHit.point);
            }
        }
    }

    [Command]
    void CmdThrowGerenade(Vector3 dest) {
        if(CurrentStamina < mGerenadeStaminaCost) 
        {
            Debug.Log("not enough stamina for gernade");
            return;
        }

        if(Time.time - lastGerenadeTime < mGerenadeIntervalSec)
        {
            Debug.Log("not enought gerenade interval" + (Time.time - lastGerenadeTime));
            return;
        }

        CurrentStamina -= mGerenadeStaminaCost;
        lastGerenadeTime = Time.time;
        GameObject gerenade = Instantiate(gerenadePrefabs[order], dest, Quaternion.identity);
        gerenade.transform.Rotate(180f * Vector3.up);
        gerenade.layer = LayerMask.NameToLayer(playerTag + "Destructable");
        gerenade.tag = playerTag;
        NetworkServer.Spawn(gerenade);
	    RpcSetLayerAndTag(
            gerenade.GetComponent<NetworkIdentity>().netId,
            gerenade.tag,
            gerenade.layer);
    }

    [Command]
    void CmdThrowKnife(Vector3 dest) {
        if(CurrentStamina < mShootStaminaCost ||
          Time.time - lastShootTime < mShootIntervalSec
        ) {
            return;
        }
        CurrentStamina -= mShootStaminaCost;
        lastShootTime = Time.time;

        GameObject projectile = Instantiate(swordPrefab,
            Camera.main.transform.position + new Vector3(10,10,10),
            Quaternion.identity) as GameObject;

        projectile.layer = LayerMask.NameToLayer(playerTag + "Objects");
        projectile.tag = playerTag;
        projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * 40;
        NetworkServer.Spawn(projectile);

        RpcSetLayerAndTag(
            projectile.GetComponent<NetworkIdentity>().netId,
            projectile.tag,
            projectile.layer);

        projectile.GetComponent<Sword>().targetPosition = dest;
        Destroy(projectile, projectileLifeTime);
    }

    [ClientRpc]
    void RpcSetLayerAndTag(NetworkInstanceId netId, string tag, int layer) {
		var obj = ClientScene.FindLocalObject(netId);
		Debug.Log("Setting layer and tag for:"+ obj);
		obj.tag = tag;
		obj.layer = layer;
    }

    [Command]
    void CmdRequestPlayerTag(string token) {
        if(playerTokens.Count < 2) { // accept player
            var playerIndex = playerTokens.IndexOf(token);
            if(playerIndex > -1) { // connected before
                this.playerTag = "P" + (playerIndex+1).ToString();
                order = playerIndex;
            } else { // new player
                playerTokens.Add(token);
                this.playerTag = "P" + playerTokens.Count;
                order = playerTokens.Count - 1;
            }
        }
    }

	void OnUpdatePlayerTag(string playerTag) {
        var opponentLayerName = "Destructable";
		this.playerTag = playerTag;
        switch (playerTag)
        {
            case("P1"):
                opponentLayerName = "P2" + opponentLayerName;
                break;
            case("P2"):
                opponentLayerName = "P1" + opponentLayerName;
                break;
        }
        destructableMask = 1 << LayerMask.NameToLayer (opponentLayerName);
	}
}
