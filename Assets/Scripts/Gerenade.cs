using UnityEngine;
using BeautifulDissolves;
using UnityEngine.Networking;

[RequireComponent(typeof(Dissolve))]
public class Gerenade : NetworkBehaviour {
    [SerializeField] GameObject m_ExplosionVfx;
    [SerializeField] float m_DenoteDelay;
    [SerializeField] Animator m_Animator;
	[SerializeField] AudioSource m_Audio;
	[SerializeField] ParticleSystem m_DeathParticles;
	[SerializeField] float m_removeCorpseDelay = 3f;
	[SerializeField] float m_vfxLifeTime = 1f;

    [SyncVar(hook="OnDefusedUpdate")]
    private bool mIsDefused = false;


    void Start() {
        Invoke("Denote", m_DenoteDelay);
    }
	public void DestroySelf()
	{
		Destroy(this.gameObject);
	}

    public void Defuse() {
        mIsDefused = true;
        OnDefused();
    }

    public void OnDefused() {
        this.GetComponent<Collider>().enabled = false;
		m_Animator.SetTrigger("Dead");
        this.GetComponent<Dissolve>().TriggerDissolve();
        m_Audio.Play();
		m_DeathParticles.Play();
        if(isServer) {
            Destroy(this.gameObject, m_removeCorpseDelay);
        }
    }

    public void OnDefusedUpdate(bool isDefused) {
        mIsDefused = isDefused;
        OnDefused();
    }
    public void Denote() {
        if(mIsDefused) {
            return;
        }
        var evfx = Instantiate(m_ExplosionVfx, this.transform.position, Quaternion.identity) as GameObject;
        Destroy(evfx, m_vfxLifeTime);
        DestroySelf();
    }
}
